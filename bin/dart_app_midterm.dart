import 'dart:ffi';
import 'dart:math';

import 'package:dart_app_midterm/dart_app_midterm.dart' as dart_app_midterm;

import 'dart:io';

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return int.tryParse(s) != null;
}

bool isOperators(String s) {
  if (s.compareTo("(") == 0 || s.compareTo(")") == 0) {
    return false;
  }
  return true;
}

int precedence(String ch) {
  switch (ch) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "%":
    case "/":
      return 2;
    case "^":
      return 3;
    default:
      return -1;
  }
}

int applyOp(String val, int left, int right) {
  switch (val) {
    case '+':
      return left + right;
    case '-':
      return left - right;
    case '*':
      return left * right;
    case '%':
      return left % right;
    case '^':
      int res = 0;
      for (int i = 0; i < right; i++) {
        res = left * left;
      }
      return res;
    case '/':
      return left ~/ right;
  }
  return 0;
}

void tokenString(String input) {
  var token;
  if (!input.contains(' ')) {
    token = input.split('');
  } else {
    token = input.split(' ');
  }
  var token0 = [];
  for (int i = 0; i < token.length; i++) {
    if (token[i] != '') {
      token0.add(token[i]);
      print(token0);
    }
  }
  print("token list : $token0");
  infixToPostfix(token0);
}

void infixToPostfix(List token0) {
  var operators = new List<String>.filled(0, "key", growable: true);
  var postfix = new List<String>.filled(0, "key", growable: true);
  for (int i = 0; i < token0.length; i++) {
    if (isNumeric(token0[i]) == true) {
      postfix.add(token0[i]);
    }
    if (isNumeric(token0[i]) == false && isOperators(token0[i])) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token0[i]) <= precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(token0[i]);
    }
    if ("(".compareTo(token0[i]) == 0) {
      operators.add(token0[i]);
    }
    if (")".compareTo(token0[i]) == 0) {
      while (operators.last.compareTo("(") != 0) {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }
  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  print("postfix : $postfix");
  evaluatePostfix(postfix);
}

void evaluatePostfix(List postfix) {
  List values = [];
  for (String val in postfix) {
    if (isNumeric(val)) {
      values.add(int.parse(val));
    } else {
      int right = values.removeLast();
      int left = values.removeLast();
      values.add(applyOp(val, left, right));
    }
  }
  print("evaluate postfix : $values");
}

void main() {
  print("Please input your data");
  String input = stdin.readLineSync()!;
  tokenString(input);
}
